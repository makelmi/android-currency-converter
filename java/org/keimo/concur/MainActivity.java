package org.keimo.concur;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.String;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;


public class MainActivity extends AppCompatActivity {

    boolean autoInput = false;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        // Get handles to layout objects
        final ImageView currency1_img = (ImageView) findViewById(R.id.currency1_img);
        final ImageView currency2_img = (ImageView) findViewById(R.id.currency2_img);

        // Set action bar
        Toolbar concurActionBar = (Toolbar) findViewById(R.id.concur_actionbar);
        setSupportActionBar(concurActionBar);

        TableLayout rate_table = (TableLayout) findViewById(R.id.rate_table);
        TableLayout rate_table_right = (TableLayout) findViewById(R.id.rate_table_right);

        TableRow AUD_row = (TableRow) rate_table.getChildAt(0);
        TableRow AUD_row2 = (TableRow) rate_table_right.getChildAt(0);

        TableRow BGN_row = (TableRow) rate_table.getChildAt(1);
        TableRow BGN_row2 = (TableRow) rate_table_right.getChildAt(1);

        TableRow BRL_row = (TableRow) rate_table.getChildAt(2);
        TableRow BRL_row2 = (TableRow) rate_table_right.getChildAt(2);

        TableRow CAD_row = (TableRow) rate_table.getChildAt(3);
        TableRow CAD_row2 = (TableRow) rate_table_right.getChildAt(3);

        TableRow CHF_row = (TableRow) rate_table.getChildAt(4);
        TableRow CHF_row2 = (TableRow) rate_table_right.getChildAt(4);

        TableRow CNY_row = (TableRow) rate_table.getChildAt(5);
        TableRow CNY_row2 = (TableRow) rate_table_right.getChildAt(5);

        TableRow CZK_row = (TableRow) rate_table.getChildAt(6);
        TableRow CZK_row2 = (TableRow) rate_table_right.getChildAt(6);

        TableRow DKK_row = (TableRow) rate_table.getChildAt(7);
        TableRow DKK_row2 = (TableRow) rate_table_right.getChildAt(7);

        TableRow EUR_row = (TableRow) rate_table.getChildAt(8);
        TableRow EUR_row2 = (TableRow) rate_table_right.getChildAt(8);

        TableRow GBP_row = (TableRow) rate_table.getChildAt(9);
        TableRow GBP_row2 = (TableRow) rate_table_right.getChildAt(9);

        TableRow HKD_row = (TableRow) rate_table.getChildAt(10);
        TableRow HKD_row2 = (TableRow) rate_table_right.getChildAt(10);

        TableRow HRK_row = (TableRow) rate_table.getChildAt(11);
        TableRow HRK_row2 = (TableRow) rate_table_right.getChildAt(11);

        TableRow HUF_row = (TableRow) rate_table.getChildAt(12);
        TableRow HUF_row2 = (TableRow) rate_table_right.getChildAt(12);

        TableRow IDR_row = (TableRow) rate_table.getChildAt(13);
        TableRow IDR_row2 = (TableRow) rate_table_right.getChildAt(13);

        TableRow ILS_row = (TableRow) rate_table.getChildAt(14);
        TableRow ILS_row2 = (TableRow) rate_table_right.getChildAt(14);

        TableRow INR_row = (TableRow) rate_table.getChildAt(15);
        TableRow INR_row2 = (TableRow) rate_table_right.getChildAt(15);

        TableRow JPY_row = (TableRow) rate_table.getChildAt(16);
        TableRow JPY_row2 = (TableRow) rate_table_right.getChildAt(16);

        TableRow KRW_row = (TableRow) rate_table.getChildAt(17);
        TableRow KRW_row2 = (TableRow) rate_table_right.getChildAt(17);

        TableRow MXN_row = (TableRow) rate_table.getChildAt(18);
        TableRow MXN_row2 = (TableRow) rate_table_right.getChildAt(18);

        TableRow MYR_row = (TableRow) rate_table.getChildAt(19);
        TableRow MYR_row2 = (TableRow) rate_table_right.getChildAt(19);

        TableRow NOK_row = (TableRow) rate_table.getChildAt(20);
        TableRow NOK_row2 = (TableRow) rate_table_right.getChildAt(20);

        TableRow NZD_row = (TableRow) rate_table.getChildAt(21);
        TableRow NZD_row2 = (TableRow) rate_table_right.getChildAt(21);

        TableRow PHP_row = (TableRow) rate_table.getChildAt(22);
        TableRow PHP_row2 = (TableRow) rate_table_right.getChildAt(22);

        TableRow PLN_row = (TableRow) rate_table.getChildAt(23);
        TableRow PLN_row2 = (TableRow) rate_table_right.getChildAt(23);

        TableRow RON_row = (TableRow) rate_table.getChildAt(24);
        TableRow RON_row2 = (TableRow) rate_table_right.getChildAt(24);

        TableRow RUB_row = (TableRow) rate_table.getChildAt(25);
        TableRow RUB_row2 = (TableRow) rate_table_right.getChildAt(25);

        TableRow SEK_row = (TableRow) rate_table.getChildAt(26);
        TableRow SEK_row2 = (TableRow) rate_table_right.getChildAt(26);

        TableRow SGD_row = (TableRow) rate_table.getChildAt(27);
        TableRow SGD_row2 = (TableRow) rate_table_right.getChildAt(27);

        TableRow THB_row = (TableRow) rate_table.getChildAt(28);
        TableRow THB_row2 = (TableRow) rate_table_right.getChildAt(28);

        TableRow TRY_row = (TableRow) rate_table.getChildAt(29);
        TableRow TRY_row2 = (TableRow) rate_table_right.getChildAt(29);

        TableRow USD_row = (TableRow) rate_table.getChildAt(30);
        TableRow USD_row2 = (TableRow) rate_table_right.getChildAt(30);

        TableRow ZAR_row = (TableRow) rate_table.getChildAt(31);
        TableRow ZAR_row2 = (TableRow) rate_table_right.getChildAt(31);

        // Make rows listen to clicks (selects currencies, calls update on editTexts and calls the tableHighlighter)
        AUD_row.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();

                editor.putString("currency1", "AUD");
                editor.apply();

                updateEditText(R.id.currency1);

                currency1_img.setImageResource(R.drawable.aud);
                tableHighlighter();
            }
        });

        AUD_row2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();

                editor.putString("currency2", "AUD");
                editor.apply();

                updateEditText(R.id.currency2);

                currency2_img.setImageResource(R.drawable.aud);
                tableHighlighter();
            }
        });

        BGN_row.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();

                editor.putString("currency1", "BGN");
                editor.apply();

                updateEditText(R.id.currency1);

                currency1_img.setImageResource(R.drawable.bgn);
                tableHighlighter();
            }
        });

        BGN_row2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();

                editor.putString("currency2", "BGN");
                editor.apply();

                updateEditText(R.id.currency2);

                currency2_img.setImageResource(R.drawable.bgn);
                tableHighlighter();
            }
        });

        BRL_row.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();

                editor.putString("currency1", "BRL");
                editor.apply();

                updateEditText(R.id.currency1);

                currency1_img.setImageResource(R.drawable.brl);
                tableHighlighter();
            }
        });

        BRL_row2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();

                editor.putString("currency2", "BRL");
                editor.apply();

                updateEditText(R.id.currency2);

                currency2_img.setImageResource(R.drawable.brl);
                tableHighlighter();

            }
        });

        CAD_row.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();

                editor.putString("currency1", "CAD");
                editor.apply();

                updateEditText(R.id.currency1);

                currency1_img.setImageResource(R.drawable.cad);
                tableHighlighter();

            }
        });

        CAD_row2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();

                editor.putString("currency2", "CAD");
                editor.apply();

                updateEditText(R.id.currency2);

                currency2_img.setImageResource(R.drawable.cad);
                tableHighlighter();

            }
        });

        CHF_row.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();

                editor.putString("currency1", "CHF");
                editor.apply();

                updateEditText(R.id.currency1);

                currency1_img.setImageResource(R.drawable.chf);
                tableHighlighter();

            }
        });

        CHF_row2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();

                editor.putString("currency2", "CHF");
                editor.apply();

                updateEditText(R.id.currency2);

                currency2_img.setImageResource(R.drawable.chf);
                tableHighlighter();

            }
        });

        CNY_row.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();

                editor.putString("currency1", "CNY");
                editor.apply();

                updateEditText(R.id.currency1);

                currency1_img.setImageResource(R.drawable.cny);
                tableHighlighter();

            }
        });

        CNY_row2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();

                editor.putString("currency2", "CNY");
                editor.apply();

                updateEditText(R.id.currency2);

                currency2_img.setImageResource(R.drawable.cny);
                tableHighlighter();

            }
        });

        CZK_row.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();

                editor.putString("currency1", "CZK");
                editor.apply();

                updateEditText(R.id.currency1);

                currency1_img.setImageResource(R.drawable.czk);
                tableHighlighter();

            }
        });

        CZK_row2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();

                editor.putString("currency2", "CZK");
                editor.apply();

                updateEditText(R.id.currency2);

                currency2_img.setImageResource(R.drawable.czk);
                tableHighlighter();

            }
        });

        DKK_row.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();

                editor.putString("currency1", "DKK");
                editor.apply();

                updateEditText(R.id.currency1);

                currency1_img.setImageResource(R.drawable.dkk);
                tableHighlighter();

            }
        });

        DKK_row2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();

                editor.putString("currency2", "DKK");
                editor.apply();

                updateEditText(R.id.currency2);

                currency2_img.setImageResource(R.drawable.dkk);
                tableHighlighter();

            }
        });

        EUR_row.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();

                editor.putString("currency1", "EUR");
                editor.apply();

                updateEditText(R.id.currency1);

                currency1_img.setImageResource(R.drawable.eur);
                tableHighlighter();

            }
        });

        EUR_row2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();

                editor.putString("currency2", "EUR");
                editor.apply();

                updateEditText(R.id.currency2);

                currency2_img.setImageResource(R.drawable.eur);
                tableHighlighter();

            }
        });

        GBP_row.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();

                editor.putString("currency1", "GBP");
                editor.apply();

                updateEditText(R.id.currency1);

                currency1_img.setImageResource(R.drawable.gbp);
                tableHighlighter();

            }
        });

        GBP_row2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();

                editor.putString("currency2", "GBP");
                editor.apply();

                updateEditText(R.id.currency2);

                currency2_img.setImageResource(R.drawable.gbp);
                tableHighlighter();

            }
        });

        HKD_row.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();

                editor.putString("currency1", "HKD");
                editor.apply();

                updateEditText(R.id.currency1);

                currency1_img.setImageResource(R.drawable.hkd);
                tableHighlighter();

            }
        });

        HKD_row2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();

                editor.putString("currency2", "HKD");
                editor.apply();

                updateEditText(R.id.currency2);

                currency2_img.setImageResource(R.drawable.hkd);
                tableHighlighter();

            }
        });

        HRK_row.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();

                editor.putString("currency1", "HRK");
                editor.apply();

                updateEditText(R.id.currency1);

                currency1_img.setImageResource(R.drawable.hrk);
                tableHighlighter();

            }
        });

        HRK_row2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();

                editor.putString("currency2", "HRK");
                editor.apply();

                updateEditText(R.id.currency2);

                currency2_img.setImageResource(R.drawable.hrk);
                tableHighlighter();

            }
        });

        HUF_row.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();

                editor.putString("currency1", "HUF");
                editor.apply();

                updateEditText(R.id.currency1);

                currency1_img.setImageResource(R.drawable.huf);
                tableHighlighter();

            }
        });

        HUF_row2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();

                editor.putString("currency2", "HUF");
                editor.apply();

                updateEditText(R.id.currency2);

                currency2_img.setImageResource(R.drawable.huf);
                tableHighlighter();

            }
        });

        IDR_row.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();

                editor.putString("currency1", "IDR");
                editor.apply();

                updateEditText(R.id.currency1);

                currency1_img.setImageResource(R.drawable.idr);
                tableHighlighter();

            }
        });

        IDR_row2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();

                editor.putString("currency2", "IDR");
                editor.apply();

                updateEditText(R.id.currency2);

                currency2_img.setImageResource(R.drawable.idr);
                tableHighlighter();

            }
        });

        ILS_row.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();

                editor.putString("currency1", "ILS");
                editor.apply();

                updateEditText(R.id.currency1);

                currency1_img.setImageResource(R.drawable.ils);
                tableHighlighter();

            }
        });

        ILS_row2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();

                editor.putString("currency2", "ILS");
                editor.apply();

                updateEditText(R.id.currency2);

                currency2_img.setImageResource(R.drawable.ils);
                tableHighlighter();

            }
        });

        INR_row.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();

                editor.putString("currency1", "INR");
                editor.apply();

                updateEditText(R.id.currency1);

                currency1_img.setImageResource(R.drawable.inr);
                tableHighlighter();

            }
        });

        INR_row2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();

                editor.putString("currency2", "INR");
                editor.apply();

                updateEditText(R.id.currency2);

                currency2_img.setImageResource(R.drawable.inr);
                tableHighlighter();

            }
        });

        JPY_row.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();

                editor.putString("currency1", "JPY");
                editor.apply();

                updateEditText(R.id.currency1);

                currency1_img.setImageResource(R.drawable.jpy);
                tableHighlighter();

            }
        });

        JPY_row2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();

                editor.putString("currency2", "JPY");
                editor.apply();

                updateEditText(R.id.currency2);

                currency2_img.setImageResource(R.drawable.jpy);
                tableHighlighter();

            }
        });

        KRW_row.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();

                editor.putString("currency1", "KRW");
                editor.apply();

                updateEditText(R.id.currency1);

                currency1_img.setImageResource(R.drawable.krw);
                tableHighlighter();

            }
        });

        KRW_row2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();

                editor.putString("currency2", "KRW");
                editor.apply();

                updateEditText(R.id.currency2);

                currency2_img.setImageResource(R.drawable.krw);
                tableHighlighter();

            }
        });

        MXN_row.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();

                editor.putString("currency1", "MXN");
                editor.apply();

                updateEditText(R.id.currency1);

                currency1_img.setImageResource(R.drawable.mxn);
                tableHighlighter();

            }
        });

        MXN_row2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();

                editor.putString("currency2", "MXN");
                editor.apply();

                updateEditText(R.id.currency2);

                currency2_img.setImageResource(R.drawable.mxn);
                tableHighlighter();

            }
        });

        MYR_row.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();

                editor.putString("currency1", "MYR");
                editor.apply();

                updateEditText(R.id.currency1);

                currency1_img.setImageResource(R.drawable.myr);
                tableHighlighter();

            }
        });

        MYR_row2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();

                editor.putString("currency2", "MYR");
                editor.apply();

                updateEditText(R.id.currency2);

                currency2_img.setImageResource(R.drawable.myr);
                tableHighlighter();

            }
        });

        NOK_row.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();

                editor.putString("currency1", "NOK");
                editor.apply();

                updateEditText(R.id.currency1);

                currency1_img.setImageResource(R.drawable.nok);
                tableHighlighter();

            }
        });

        NOK_row2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();

                editor.putString("currency2", "NOK");
                editor.apply();

                updateEditText(R.id.currency2);

                currency2_img.setImageResource(R.drawable.nok);
                tableHighlighter();

            }
        });

        NZD_row.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();

                editor.putString("currency1", "NZD");
                editor.apply();

                updateEditText(R.id.currency1);

                currency1_img.setImageResource(R.drawable.nzd);
                tableHighlighter();

            }
        });

        NZD_row2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();

                editor.putString("currency2", "NZD");
                editor.apply();

                updateEditText(R.id.currency2);

                currency2_img.setImageResource(R.drawable.nzd);
                tableHighlighter();

            }
        });

        PHP_row.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();

                editor.putString("currency1", "PHP");
                editor.apply();

                updateEditText(R.id.currency1);

                currency1_img.setImageResource(R.drawable.php);
                tableHighlighter();

            }
        });

        PHP_row2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();

                editor.putString("currency2", "PHP");
                editor.apply();

                updateEditText(R.id.currency2);

                currency2_img.setImageResource(R.drawable.php);
                tableHighlighter();

            }
        });

        PLN_row.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();

                editor.putString("currency1", "PLN");
                editor.apply();

                updateEditText(R.id.currency1);

                currency1_img.setImageResource(R.drawable.pln);
                tableHighlighter();

            }
        });

        PLN_row2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();

                editor.putString("currency2", "PLN");
                editor.apply();

                updateEditText(R.id.currency2);

                currency2_img.setImageResource(R.drawable.pln);
                tableHighlighter();

            }
        });

        RON_row.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();

                editor.putString("currency1", "RON");
                editor.apply();

                updateEditText(R.id.currency1);

                currency1_img.setImageResource(R.drawable.ron);
                tableHighlighter();

            }
        });

        RON_row2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();

                editor.putString("currency2", "RON");
                editor.apply();

                updateEditText(R.id.currency2);

                currency2_img.setImageResource(R.drawable.ron);
                tableHighlighter();

            }
        });

        RUB_row.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();

                editor.putString("currency1", "RUB");
                editor.apply();

                updateEditText(R.id.currency1);

                currency1_img.setImageResource(R.drawable.rub);
                tableHighlighter();

            }
        });

        RUB_row2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();

                editor.putString("currency2", "RUB");
                editor.apply();

                updateEditText(R.id.currency2);

                currency2_img.setImageResource(R.drawable.rub);
                tableHighlighter();

            }
        });

        SEK_row.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();

                editor.putString("currency1", "SEK");
                editor.apply();

                updateEditText(R.id.currency1);

                currency1_img.setImageResource(R.drawable.sek);
                tableHighlighter();

            }
        });

        SEK_row2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();

                editor.putString("currency2", "SEK");
                editor.apply();

                updateEditText(R.id.currency2);

                currency2_img.setImageResource(R.drawable.sek);
                tableHighlighter();

            }
        });

        SGD_row.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();

                editor.putString("currency1", "SGD");
                editor.apply();

                updateEditText(R.id.currency1);

                currency1_img.setImageResource(R.drawable.sgd);
                tableHighlighter();

            }
        });

        SGD_row2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();

                editor.putString("currency2", "SGD");
                editor.apply();

                updateEditText(R.id.currency2);

                currency2_img.setImageResource(R.drawable.sgd);
                tableHighlighter();

            }
        });

        THB_row.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();

                editor.putString("currency1", "THB");
                editor.apply();

                updateEditText(R.id.currency1);

                currency1_img.setImageResource(R.drawable.thb);
                tableHighlighter();

            }
        });

        THB_row2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();

                editor.putString("currency2", "THB");
                editor.apply();

                updateEditText(R.id.currency2);

                currency2_img.setImageResource(R.drawable.thb);
                tableHighlighter();

            }
        });

        TRY_row.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();

                editor.putString("currency1", "TRY");
                editor.apply();

                updateEditText(R.id.currency1);

                currency1_img.setImageResource(R.drawable.try1);
                tableHighlighter();

            }
        });

        TRY_row2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();

                editor.putString("currency2", "TRY");
                editor.apply();

                updateEditText(R.id.currency2);

                currency2_img.setImageResource(R.drawable.try1);
                tableHighlighter();

            }
        });

        USD_row.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();

                editor.putString("currency1", "USD");
                editor.apply();

                updateEditText(R.id.currency1);

                currency1_img.setImageResource(R.drawable.usd);
                tableHighlighter();

            }
        });

        USD_row2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();

                editor.putString("currency2", "USD");
                editor.apply();

                updateEditText(R.id.currency2);

                currency2_img.setImageResource(R.drawable.usd);
                tableHighlighter();

            }
        });

        ZAR_row.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();

                editor.putString("currency1", "ZAR");
                editor.apply();

                updateEditText(R.id.currency1);

                currency1_img.setImageResource(R.drawable.zar);
                tableHighlighter();

            }
        });

        ZAR_row2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();

                editor.putString("currency2", "ZAR");
                editor.apply();

                updateEditText(R.id.currency2);

                currency2_img.setImageResource(R.drawable.zar);
                tableHighlighter();

            }
        });

        // Get handles to editText-boxes
        final EditText currency1TextBox = (EditText) findViewById(R.id.currency1);
        final EditText currency2TextBox = (EditText) findViewById(R.id.currency2);


        // Set changelisteners to editText-boxes
        currency1TextBox.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                // Ignore programmatic changes
                if(autoInput) {
                    autoInput = false;
                    return;
                }

                // Get sharedPreferences, load selected currencies
                SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);

                String currency1 = sharedPref.getString("currency1", "");
                String currency2 = sharedPref.getString("currency2", "");

                // Get conversion rates, load selected rates
                sharedPref = getApplicationContext().getSharedPreferences(getString(R.string.conversion_rates_file_key), Context.MODE_PRIVATE);

                float rate1 = sharedPref.getFloat(currency1, 0);
                float rate2 = sharedPref.getFloat(currency2, 0);
                float enteredValue;

                // Clean entered string (remove multiple decimal points, leading decimal point etc.)
                if(s.length() > 0) {

                    String s2 = s.toString().replace(',', '.');
                    s = s2;

                    StringBuilder sb = new StringBuilder(s);

                    while(sb.toString().indexOf('.') != sb.toString().lastIndexOf('.')) {
                        sb.deleteCharAt(sb.toString().lastIndexOf('.'));
                    }

                    s = sb.toString();

                    if (s.charAt(0) == '.') {

                        s2 = "0" + s;
                        s = s2;
                    }

                    if(s.charAt(s.length()-1) == '.') {
                        return;
                    }

                    enteredValue = Float.parseFloat(s.toString());
                } else return;

                // Calculate conversion multiplier between selected currencies & convert value
                double conversionMultiplier = rate2 / rate1;
                double conversion = enteredValue * conversionMultiplier;

                // Format decimal string
                DecimalFormat df = new DecimalFormat("#.##");
                String converted = df.format(conversion);

                // Set the editText with the updated value and set ignore flag to disable textwatcher
                autoInput = true;
                currency2TextBox.setText(converted);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        // Same stuff to the other editText
        currency2TextBox.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if(autoInput) {
                    autoInput = false;
                    return;
                }

                SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);

                String currency1 = sharedPref.getString("currency1", "");
                String currency2 = sharedPref.getString("currency2", "");

                sharedPref = getApplicationContext().getSharedPreferences(getString(R.string.conversion_rates_file_key), Context.MODE_PRIVATE);

                float rate1 = sharedPref.getFloat(currency1, 0);
                float rate2 = sharedPref.getFloat(currency2, 0);
                float enteredValue;

                if(s.length() > 0) {

                    String s2 = s.toString().replace(',', '.');
                    s = s2;

                    StringBuilder sb = new StringBuilder(s);

                    while(sb.toString().indexOf('.') != sb.toString().lastIndexOf('.')) {
                        sb.deleteCharAt(sb.toString().lastIndexOf('.'));
                    }

                    s = sb.toString();

                    if (s.charAt(0) == '.') {

                        s2 = "0" + s;
                        s = s2;
                    }

                    if(s.charAt(s.length()-1) == '.') {
                        return;
                    }

                    enteredValue = Float.parseFloat(s.toString());
                } else return;

                double conversionMultiplier = rate1 / rate2;
                double conversion = enteredValue * conversionMultiplier;

                DecimalFormat df = new DecimalFormat("#.##");
                String converted = df.format(conversion);

                autoInput = true;
                currency1TextBox.setText(converted);

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        // Set defaults if sharedPreferences is not set before (=first run)
        SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);

        if(!sharedPref.contains("currency1")) {

            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putString("currency1", "EUR");
            editor.putString("currency2", "USD");
            editor.putString("update_date", "0000-00-00");
            editor.putString("download_date", "0000-00-00");
            editor.apply();

            sharedPref = getApplicationContext().getSharedPreferences(getString(R.string.conversion_rates_file_key), Context.MODE_PRIVATE);

            editor = sharedPref.edit();

            editor.putFloat("EUR", (float)1.0);
            editor.apply();
        }
    }

    @Override
    public void onResume() {

        super.onResume();

        SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        Date strUpdateDate;

        Date today = new Date();

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(today);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        today = calendar.getTime();

        // Check if current date is after the last update date and update if so
        try {
            strUpdateDate = sdf.parse(sharedPref.getString("update_date", null));

            if (today.after(strUpdateDate)) {
                    refreshButtonHandler(getCurrentFocus());
            } else {
                updateTextViews();
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }

        // Update layout items to match previous selections and highlight tablerows accordingly
        updateEditText(0);
        tableHighlighter();
    }


    // Refreshbuttonhandler handles every requested update to currency info
    public void refreshButtonHandler(View view){

        String baseCurrency = "EUR";
        String stringUrl = "http://api.fixer.io/latest?base=" + baseCurrency;
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        // Check for connection and create a new thread to execute JSON download
        if(networkInfo != null && networkInfo.isConnected()) {
            new currencyDataDownloadTask().execute(stringUrl);
        }
        else {
            Toast.makeText(getApplicationContext(), "No connection.", Toast.LENGTH_LONG).show();
            updateTextViews();
        }
    }

    // Asynctask run on a separate thread
    private class currencyDataDownloadTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... urls) {

            // Starts downloadtask with given URL, resulting string is handled on onPostExecute()
            try {
                return downloadData(urls[0]);
            } catch(IOException e) {
                return "Unable to retrieve web page. URL may be invalid.";
            }
        }

        // onPostExecute calls JSONparser and resets editTexts
        @Override
        protected void onPostExecute(String result) {

            String toastString = "Currency data refreshed.";
            SharedPreferences sharedPrefs = getApplicationContext().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);

            String previousUpdateDate = sharedPrefs.getString("update_date", null);

            parseJSONString(result);

            if(previousUpdateDate == sharedPrefs.getString("update_date", null)) {
                toastString = "No new data. ECB updates MON-FRI 16:00 CET.";
            }

            Toast.makeText(getApplicationContext(), toastString, Toast.LENGTH_LONG).show();

            final EditText currency1TextBox = (EditText) findViewById(R.id.currency1);
            final EditText currency2TextBox = (EditText) findViewById(R.id.currency2);

            currency1TextBox.setText("1");
            currency2TextBox.setText("1");
            updateEditText(R.id.currency2);
        }
    }

    // GET requested data from API server, return the resulting inputstream -> string (reader)
    private String downloadData(String fixerUrl) throws IOException {

        InputStream is = null;

        try {
            URL url = new URL(fixerUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.connect();
            is = conn.getInputStream();

            return reader(is);

        } finally {
            if(is != null) {
                is.close();
            }
        }
    }

    // Create a string from inputstream
    public String reader(InputStream stream) throws IOException {

        Reader reader = new InputStreamReader(stream);
        char[] buffer = new char[500];
        reader.read(buffer);
        return new String(buffer);
    }

    // Parse downloaded JSON string and update preference files with refreshed data
    public void parseJSONString(String jsonString) {

        try {
            SharedPreferences conversionRates = getApplicationContext().getSharedPreferences(getString(R.string.conversion_rates_file_key), Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = conversionRates.edit();

            JSONObject jo = new JSONObject(jsonString);
            JSONObject ja = jo.getJSONObject("rates");
            Iterator<String> i = ja.keys();

            // Iterate through JSON rates and set TextViews
            while(i.hasNext()) {

                String currency = i.next();
                float rate = (float)ja.getDouble(currency);

                String tvid = currency + "_rate";

                int resId = getResources().getIdentifier(tvid, "id", getPackageName());

                if(resId > 0) {

                    TextView crtv = (TextView) findViewById(resId);
                    crtv.setText(Float.toString(rate));
                }

                tvid = currency + "_rate2";

                resId = getResources().getIdentifier(tvid, "id", getPackageName());

                if(resId > 0) {

                    TextView crtv = (TextView) findViewById(resId);
                    crtv.setText(Float.toString(rate));
                }

                editor.putFloat(currency, rate);
            }

            editor.apply();

            SharedPreferences sharedPrefs = getApplicationContext().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
            editor = sharedPrefs.edit();

            Calendar cal = Calendar.getInstance();

            int month = cal.get(Calendar.MONTH) + 1;
            String month_string;

            if(month < 10) {
                month_string = "0" + Integer.toString(month);
            } else {
                month_string = Integer.toString(month);
            }

            // Update download and upload dates to preference file
            String dl_date = cal.get(Calendar.YEAR) + "-" + month_string + "-" + cal.get(Calendar.DATE);

            editor.putString("update_date", jo.getString("date"));
            editor.putString("download_date", dl_date);
            editor.apply();

            TextView udtv = (TextView)findViewById(R.id.update_date_TextView);
            String udtvS = getString(R.string.update_text) + sharedPrefs.getString("update_date", null) + getString(R.string.ecb);

            udtv.setText(udtvS);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    // Create actionbar menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.concur_actionbar, menu);
        return true;
    }

    // Handle actionbar clicks
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.action_refresh:
                refreshButtonHandler(getCurrentFocus());
                return true;

            case R.id.action_about:
                aboutButtonHandler(getCurrentFocus());
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


    // Handle textview and imageview changes on resume, currency change etc.
    public void updateEditText(int id) {

        SharedPreferences sharedpref = getApplicationContext().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);

        EditText et1 = (EditText) findViewById(R.id.currency1);
        EditText et2 = (EditText) findViewById(R.id.currency2);

        String currency1 = sharedpref.getString("currency1", "");
        String currency2 = sharedpref.getString("currency2", "");

        sharedpref = getApplicationContext().getSharedPreferences(getString(R.string.conversion_rates_file_key), Context.MODE_PRIVATE);

        if(id == R.id.currency1) {

            StringBuilder et2Sb = new StringBuilder(et2.getText().toString());

            while(et2Sb.toString().charAt(et2Sb.toString().length()-1) == '.') {
                et2Sb.deleteCharAt(et2Sb.toString().length()-1);
            }

            float previousValue2 = Float.parseFloat(et2Sb.toString());
            float rate1 = sharedpref.getFloat(currency1, 0);
            float rate2 = sharedpref.getFloat(currency2, 0);

            float rateMultiplier = rate1 / rate2;
            float conversion = previousValue2 * rateMultiplier;

            DecimalFormat df = new DecimalFormat("#.##");
            String converted = df.format(conversion);

            autoInput = true;

            et1.setText(converted);
        }

        else if(id == R.id.currency2) {

            StringBuilder et1Sb = new StringBuilder(et1.getText().toString());

            while(et1Sb.toString().charAt(et1Sb.toString().length()-1) == '.') {
                et1Sb.deleteCharAt(et1Sb.toString().length()-1);
            }

            float previousValue1 = Float.parseFloat(et1Sb.toString());
            float rate1 = sharedpref.getFloat(currency1, 0);
            float rate2 = sharedpref.getFloat(currency2, 0);

            float rateMultiplier = rate2 / rate1;
            float conversion = previousValue1 * rateMultiplier;

            DecimalFormat df = new DecimalFormat("#.##");
            String converted = df.format(conversion);

            autoInput = true;

            et2.setText(converted);
        }

        else {

            float rate1 = sharedpref.getFloat(currency1, 0);
            float rate2 = sharedpref.getFloat(currency2, 0);
            float rateMultiplier = rate2 / rate1;
            float conversion = 1 * rateMultiplier;

            DecimalFormat df = new DecimalFormat("#.##");
            String converted = df.format(conversion);

            et1.setText("1");
            et2.setText(converted);

            ImageView currency1_img = (ImageView) findViewById(R.id.currency1_img);
            ImageView currency2_img = (ImageView) findViewById(R.id.currency2_img);

            String idString;

            if(currency1 == "TRY") {
                idString = currency1.toLowerCase();
                idString += "1";
            }

            else {
                idString = currency1.toLowerCase();
            }

            int resId = getResources().getIdentifier(idString, "drawable", getPackageName());

            currency1_img.setImageResource(resId);

            if(currency2 == "TRY") {
                idString = currency2.toLowerCase();
                idString += "1";
            }

            else {
                idString = currency2.toLowerCase();
            }

            resId = getResources().getIdentifier(idString, "drawable", getPackageName());

            currency2_img.setImageResource(resId);
        }

        tableHighlighter();
    }


    // updateTextViews() updates the layout textviews with stored information if new download is not necessary or connection is unavailable
    public void updateTextViews() {
        SharedPreferences sp = getApplicationContext().getSharedPreferences(getString(R.string.conversion_rates_file_key), Context.MODE_PRIVATE);

        Map<String, ?> keys = sp.getAll();

        for(Map.Entry<String, ?> entry : keys.entrySet()) {

            String resString1 = entry.getKey() + "_rate";
            String resString2 = resString1 + "2";
            float rate = Float.parseFloat(entry.getValue().toString());

            int resId = getResources().getIdentifier(resString1, "id", getPackageName());
            TextView tv = (TextView) findViewById(resId);

            tv.setText(Float.toString(rate));

            resId = getResources().getIdentifier(resString2, "id", getPackageName());
            tv = (TextView) findViewById(resId);

            tv.setText(Float.toString(rate));
        }

        sp = getApplicationContext().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);

        TextView udtv = (TextView) findViewById(R.id.update_date_TextView);

        udtv.setText(getString(R.string.update_text) + sp.getString("update_date", "") + getString(R.string.ecb));
    }

    // Handle about button click and create intent to aboutActivity
    public void aboutButtonHandler(View view) {
        Intent intent = new Intent(this, AboutActivity.class);
        startActivity(intent);
    }

    // Tablehighlighter sets tablerow background colors to green on selected currencies
    public void tableHighlighter() {

        SharedPreferences sp = getApplicationContext().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);

        TableLayout tableLeft = (TableLayout) findViewById(R.id.rate_table);
        TableLayout tableRight = (TableLayout) findViewById(R.id.rate_table_right);
        ScrollView sv1 = (ScrollView) findViewById(R.id.currency_scrollview_left);
        ScrollView sv2 = (ScrollView) findViewById(R.id.currency_scrollview_right);

        int tableHeight = sv1.getHeight();

        for(int i = 0; i < tableLeft.getChildCount(); i++) {
            tableLeft.getChildAt(i).setBackgroundColor(Color.TRANSPARENT);
        }

        for(int i = 0; i < tableRight.getChildCount(); i++) {
            tableRight.getChildAt(i).setBackgroundColor(Color.TRANSPARENT);
        }

        String currencyRowString = sp.getString("currency1", "") + "_row";
        int resId = getResources().getIdentifier(currencyRowString, "id", getPackageName());

        TableRow tr = (TableRow) findViewById(resId);
        tr.setBackgroundColor(Color.LTGRAY);

        sv1.smoothScrollTo(0, tr.getBottom() - (tableHeight / 2));

        currencyRowString = sp.getString("currency2", "") + "_row2";
        resId = getResources().getIdentifier(currencyRowString, "id", getPackageName());

        tr = (TableRow) findViewById(resId);
        tr.setBackgroundColor(Color.LTGRAY);

        sv2.smoothScrollTo(0, tr.getBottom() - (tableHeight / 2));
    }
}